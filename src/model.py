from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey, Column, Integer, UnicodeText, String, Boolean
from sqlalchemy.orm import relation, backref

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    
    id = Column(Integer, primary_key=True)
    name = Column(String)
    password = Column(String)
    role = Column(String)
    
    def __init__(self, name, password, role="writer"):
        self.name = name
        self.password = password
        self.role = role
        
    def __repr__(self):
        return "<User('%s','%s','%s')>" % (self.name, self.password, self.role)


class Session(Base):
    __tablename__ = 'sessions'
    
    id = Column(Integer, primary_key=True)
    name = Column(String)
    creator_id = Column(Integer, ForeignKey('users.id'))
    creator = relation(User, backref=backref('sessions', order_by=id))
    current_round_index = Column(Integer)
    total_rounds = Column(Integer)
    
    
    def __init__(self, name, creator, total_rounds=10):
        self.name = name
        self.creator = creator
        self.current_round_index = 1
        self.total_rounds = total_rounds
        
    def __repr__(self):
        return "<Session('%s','%s','%s','%s')>" % (self.name, self.creator, 
                self.current_round, self.total_rounds)
        
        
class Round(Base):
    __tablename__ = 'rounds'
    
    id = Column(Integer, primary_key=True)
    text_out = Column(UnicodeText)
    author_id = Column(Integer, ForeignKey('users.id'))
    author = relation(User, backref=backref('rounds', order_by=id))
    session_id = Column(Integer, ForeignKey('sessions.id'))
    session = relation(Session, backref=backref('rounds', order_by=id))
    round_index = Column(Integer)
    length_seconds = Column(Integer)
    algorithm = Column(String)
    
    def __init__(self, session, length_seconds="360", algorithm="roundrobin"):
        self.session = session
        self.round_index = session.current_round_index
        self.length_seconds = length_seconds
        self.algorithm = algorithm
        
    def __repr__(self):
        return "<Round('%s','%s','%s','%s','%s')>" % (self.author, 
                self.session, self.length_seconds, self.algorithm, 
                self.text_out)

        
class Chunk(Base):
    __tablename__ = 'chunks'
    
    id = Column(Integer, primary_key=True)
    text = Column(UnicodeText)
    author_id = Column(Integer, ForeignKey('users.id'))
    author = relation(User, backref=backref('chunks', order_by=id))
    session_id = Column(Integer, ForeignKey('sessions.id'))
    session = relation(Session, backref=backref('chunks', order_by=id))
    round_id = Column(Integer, ForeignKey('rounds.id'))
    round = relation(Round, backref=backref('chunks', order_by=id))
    selected = Column(Boolean)
    
    def __init__(self, text, author, session, round):
        self.text = text
        self.author = author
        self.session = session
        self.round = round
        
    def __repr__(self):
        return "<Chunk('%s','%s','%s')>" % (self.author, self.round, 
                self.session, self.text)
        
        
class Prompt(Base):
    __tablename__ = 'prompts'
    
    id = Column(Integer, primary_key=True)
    text = Column(UnicodeText)
    author_id = Column(Integer, ForeignKey('users.id'))
    author = relation(User, backref=backref('prompts', order_by=id))
    session_id = Column(Integer, ForeignKey('sessions.id'))
    session = relation(Session, backref=backref('prompts', order_by=id))
    round_id = Column(Integer, ForeignKey('rounds.id'))
    round = relation(Round, backref=backref('prompts', order_by=id))
    selected = Column(Boolean)
    round_index = Column(Integer)
    
    def __init__(self, text, author, session, round):
        self.text = text
        self.author = author
        self.session = session
        self.round = round
        self.selected = False
        
    def __repr__(self):
        return "<Prompt('%s','%s','%s','%s')>" % (self.author, self.round, 
                self.session, self.text)

