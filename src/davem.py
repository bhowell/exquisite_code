import re,math

# Dave's munger

text = """
<p>Presumably, you would do better to understand the circumstances if you began 
by getting down on all fours. It's a good floor for that sort of thing, i think.
 Not so comfortable on the knees, perhaps, but clean. And flat. And reflective. 
 Often the relative reflectiveness of the floor gets forgotten, at least in the 
 later retellings, and it might be useful, if you really hope to understand the 
 perspective from which Janet Precept Felligrew experienced the subsequent 
 events, if you take a minute to reflect, if you will, on just how reflective 
 the floor was.</p>
<p>So from there, and please do take care of your knees, you can see how the 
lower back problems which often begin to impact us around the age of 30 and -- 
yes i know, you and i both want it to be later, but i think 30 is already a 
generous point at which to set the general onset of back problems -- might also 
serve as a distraction from the main events, and we have to recognize the ways 
in which a neck, even a neck that has been evolved to sit at the end, or the 
top, or in the corner perhaps, of a quadrapedal torso, is limited by the 
position, and to honestly encounter the story as it has been understood up 
until now, it might be useful to think about what it means to be standing -- 
not lying on your belly mind you, getting a back massage, but standing -- and 
to have your back making parallel lines with the ceiling and for the wall to be 
the thing that sits on top of your head.</p>
           """

def strip_tags(value):
    "Return the given HTML with all tags stripped."
    return re.sub(r'<[^>]*?>', '', value)

# ${\small \textnormal{\useTextGlyph{fxl}{Delta}}}$
def munge(text):
    splitted = strip_tags(text).split(" ")
    print splitted
    processed = []
    linecount = 0
    for i in range(0,len(splitted)):
        if i%2==0:
            processed.append("\n")
            for s in range(0,(math.sin(i*0.1)+1)*5):
                processed.append(" ")
            linecount=linecount+1
        processed.append(splitted[i])
    
    return "<pre>"+" ".join(processed)+"</pre>"
        

