from model import Chunk

class Testchunk():
    def __init__(self):
        self.text = ""
        
_c1 = Testchunk()
_c1.text = """
<p>Presumably, you would do better to understand the circumstances if you 
began by getting down on all fours. It's a good floor for that sort of thing, 
i think. Not so comfortable on the knees, perhaps, but clean. And flat. And 
reflective. Often the relative reflectiveness of the floor gets forgotten, 
at least in the later retellings, and it might be useful, if you really hope 
to understand the perspective from which Janet Precept Felligrew experienced 
the subsequent events, if you take a minute to reflect, if you will, on just 
how reflective the floor was.</p>
<p>So from there, and please do take care of your knees, you can see how the 
lower back problems which often begin to impact us around the age of 30 and -- 
yes i know, you and i both want it to be later, but i think 30 is already a 
generous point at which to set the general onset of back problems -- might 
also serve as a distraction from the main events, and we have to recognize 
the ways in which a neck, even a neck that has been evolved to sit at the 
end, or the top, or in the corner perhaps, of a quadrapedal torso, is limited 
by the position, and to honestly encounter the story as it has been understood 
up until now, it might be useful to think about what it means to be standing -- 
not lying on your belly mind you, getting a back massage, but standing -- and 
to have your back making parallel lines with the ceiling and for the wall to 
be the thing that sits on top of your head.</p>
           """

_c2 = Testchunk()
_c2.text = """
<p>the moon was almost full and her eyes were almost blue. this was her first 
time at a honky tonk bar and the men were not at all what she had expected. she 
thought surely there would be very tight jeans and cowboy hats and bootts but 
these men were dressed like your average construction worker...carhart pants 
and baseball caps. and the music was nothing like the hank williams and willie 
nelson she'd been hoping for. it was vince gill and shania twain and it was 
awwful frankly. starla stared at the men around her and willed them to ask her 
to dance. ask me god damnit. she thought this but was actually smiling coyly 
and sipping her roy rogers. next drink will be a gimlet she thought. a 
mechanical bull started somewhere in the back of the room and starla turned 
around to see a woman in a tube top--god how typical--flinging her arms back 
and howling her yeehaw howl. when she gets off maybe i'll get on. starla 
thought about the power of visibility in these desperate pick up situations and 
she thought about how visible she was at that moment. she was wearing red, 
tight clothes, and her hair was very voluptuous. she had a good deal of makeup 
on and glittery earrings. her age was a factor. she was 47 and she looked it. 
the makeup helped a little but basically she looked like a woman who has grown 
children and maybe the men in here knew that despite the complimentary 
lighting. ONe man had a blue sweatshirt and gray hair and starla focused all 
her sick desperate energy on him. she scanned the room again and then got up to 
go to the bar. her high heels clicking like teeth.
</p>
           """

_c3 = Testchunk()
_c3.text = """
<p>Ever since Oi was a young man he had been headhunted by various publishing, 
dictionary, thesaurus and other language based businesses. You see, Oi (whose 
last name was Euna) was the proud bearer of the shortest name in the world that 
contained all of the vowels of the English language. They sought him out 
because they guessed with his unique exposure to this dimension of the English 
language, he would have peculiar linguistic, possibly poetic qualities. Oi grew 
up as many child stars would have, having money and job offers thrown at him 
left and write. He didn't succomb so easily to the temptations, to the money or 
the fame promised to him and ended up taking an unlikely job working for a 
company that wrote the short texts we find in fortune cookies. He could have 
had everything that most of us dreamed of, but decided to move to the small 
island in South East Asia where the fortune cookie publishing company was.
</p>
           """

testchunks = [_c1, _c2, _c3]
