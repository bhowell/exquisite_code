import os
import glob
import re
import random

current_dir = os.path.dirname(os.path.abspath(__file__))    
tmpldir = os.path.join(current_dir, "templates")
datadir = os.path.join(current_dir, "data")
outfile = open(os.path.join(datadir, "sens.txt"), "w")

for i in range(10):
    path = os.path.join(datadir, str(i))
    path += "/*.txt"
    print path
    files = glob.glob(path)
    print files
    for f in files:
        tmp = open(f)
        txt = re.sub(r'<[^>]*?>', '', tmp.read())
        sens = txt.split(".")
        for sen in sens:
            sen = sen.strip()
            if sen != "":
                rand = str(random.randint(0,99999)) + ".txt"
                filepath = os.path.join(datadir, "sentsout", rand)
                fp = open(filepath, "w")
                fp.write(sen)
                fp.close()
            
outfile.close()
