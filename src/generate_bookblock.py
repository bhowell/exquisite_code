#!/usr/bin/python

import sqlite3

db = sqlite3.connect("excode.db")

fp = open("test.out", "w")

cur = db.cursor()

for row in cur.execute("select text_out, algorithm from rounds"):
    if(row[0]):
        outchunk = '<table><tr><td class="bullet">' + "&#10087;" + '</td><td class="chunk">' + row[0] + "</td></tr></table>"
        fp.write(outchunk)
