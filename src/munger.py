import random
import markov as markovo
import re
import xtextm
import davem

def strip_tags(value):
    "Return the given HTML with all tags stripped."
    return re.sub(r'<[^>]*?>', '', value)

# ${\small \textnormal{\useTextGlyph{fxl}{Rfraktur}}}$
def roundrobin(chunks):
    chunk = random.choice(chunks)
    chunk.selected = True
    return chunk.text
    
def markov(chunks):
    mark = markovo.Markov(chunks)
    return "<p>" + mark.generate_markov_text() + "</p>"

# ${\small \textnormal{\useTextGlyph{fxl}{uni0239}}}$
def twocolumnrobin(chunks):
    chunk1 = random.choice(chunks)
    chunk2 = random.choice(chunks)
    out = '<div style="width: 40%; padding: 4%; float: left">' + \
		chunk1.text + '</div>'
    out += '<div style="width: 40%; padding: 4%; float: left">' + \
		chunk2.text + '</div>'
    return out
    
# ${\small \textnormal{\useTextGlyph{fxl}{beta}}}$
def burroughs(chunks):
    text1 = strip_tags(random.choice(chunks).text)
    text2 = strip_tags(random.choice(chunks).text)
    text3 = strip_tags(random.choice(chunks).text)
    length = len(text1)
    if(len(text2) < length):
        length = len(text2)
    if(len(text3) < length):
        length = len(text3)
        
    out = ""
    
    for i in range(0, length-40, 80):
        tmp = text1[i:i+26]
        if (len(tmp)==len(tmp.rstrip())):
            cutout = tmp.split()[-1]
            tmp = tmp.rstrip(cutout)
        if (len(tmp)==len(tmp.lstrip())):
            cutout = tmp.split()[0]
            tmp = tmp.lstrip(cutout)
        out += tmp
        tmp = text2[i+27:i+54]
        if (len(tmp)==len(tmp.rstrip())):
            cutout = tmp.split()[-1]
            tmp = tmp.rstrip(cutout)
        if (len(tmp)==len(tmp.lstrip())):
            cutout = tmp.split()[0]
            tmp = tmp.lstrip(cutout)
        out += tmp
        tmp = text3[i+55:i+79]
        if (len(tmp)==len(tmp.rstrip())):
            cutout = tmp.split()[-1]
            tmp = tmp.rstrip(cutout)
        if (len(tmp)==len(tmp.lstrip())):
            cutout = tmp.split()[0]
            tmp = tmp.lstrip(cutout)
        out += tmp
    return "<p>" + out + "</p>"

def xtext(chunks):
    return xtextm.xtext(chunks)

def dave(chunks):
    chunk = random.choice(chunks)
    return davem.munge(chunk.text)

# ${\small \textnormal{\useTextGlyph{fxl}{uni01AE}}}$
def txtr(chunks):
    chunk = strip_tags(random.choice(chunks).text)[0:150]
    chunk = chunk.replace("a", "")
    chunk = chunk.replace("e", "")
    chunk = chunk.replace("i", "")
    chunk = chunk.replace("o", "")
    chunk = chunk.replace("u", "")
    chunk = "<p>" + chunk + " L8R!</p>"
    return chunk.upper()
